import React from "react";
import firebase from "../Util/firebase";
import { useState, useEffect } from "react";
import { Button, Table } from "react-bootstrap";
import Chrono from './Chrono'

const Pauses = () => {
  const [Pauses, setPauses] = useState([]);
  const [Ha, setHa] = useState();
  ///////////////////////////////////////
  const ABC = () => {
    
  }
  /////////////////////////////////////////
  const isDone = async (B) => {
    firebase.firestore()
    .collection("Utilisateurs")
    .orderBy("timeFireBase", "asc")
    .where("EnPause", "==", false)
    .limit(1)
    .onSnapshot((Users) => {
      Users.docs.forEach((U)=> setHa(U))
    })

    firebase.firestore().collection("/GestionPauses").add({
      name: Ha.data().name,
      EnPause: true,
      DatePause: firebase.firestore.FieldValue.serverTimestamp(),
      EnTerminer: false,
      DateTerminer: null,
    });




      const db = firebase.firestore()
      const Modifier = db.batch()
      const DocumentA = db.collection("Utilisateurs").doc(Ha.id)
      Modifier.update(DocumentA, {
        EnPause: true,
      })
    /////////////////////////////

    const Document = db.collection("GestionPauses").doc(B.id)
    Modifier.update(Document, {
      EnPause: false,
      EnTerminer: true,
      DateTerminer: firebase.firestore.FieldValue.serverTimestamp(),
    })
    await Modifier.commit();
  }

  useEffect(() => {
    firebase
      .firestore()
      .collection("GestionPauses")
      .orderBy("DatePause", "desc")
      .where("EnPause", "==", true)
      .onSnapshot((users) => {
        setPauses(users.docs);
      });
  }, []);
  return (
    <div className="Table">
      <Table striped bordered hover>
        <tbody>
          <tr>
            <td className="tg-ycr8"><strong>Name</strong></td>
            <td className="tg-i81m"><strong>TimePauseFB</strong></td>
            <td className="tg-i81m"><strong>Chrono</strong></td>
            <td className="tg-i81m"><strong>Action</strong></td>
            <td className="tg-i81m"><strong>Action</strong></td>
          </tr>
        </tbody>
        <tbody>
          {Pauses.map((U) => (
            <tr key={U.data().id}>
              <td>{U.data().name}</td>
              <td className="tg-ycr8">
                {U.data({ serverTimestamps: "estimate" })
                  .DatePause.toDate().toTimeString().substring(0, 12)}{" "}
              </td>
              <td ><Chrono key={U.id} Time={U.data().DatePause == null ? "charger" : U.data({ serverTimestamps: 'estimate' }).DatePause.seconds} /></td>
              <td className="tg-ycr8">
                <Button key={U.id} variant="outline-primary" onClick={() => isDone(U)}>Terminer</Button>
              </td>
              <td className="tg-ycr8">
                <Button key={U.id} variant="outline-primary" onClick={() => ABC()}>Test</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Pauses;
