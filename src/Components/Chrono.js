import React from 'react'
import firebase from "../Util/firebase";
import { useEffect, useState } from 'react'

const Chrono = ({ Time }) => {
    const [chrono, setChrono] = useState('')
    const [color, setColor] = useState('steelblue')
    const timer = (Sec) => {
        var Minutes = Math.floor(Sec / 60)
        var Seconds = Sec % 60
        return Minutes + ":" + (Seconds < 10 ? "0" : "") + Seconds;
    }
    useEffect(() => {
        var A = firebase.firestore.Timestamp.now().seconds - Number(Time)
        if(A>120) setColor('red')
        var Heeey = setInterval(() => {
            setChrono(timer(A))
        }, 1000)
        return () => {
            clearInterval(Heeey)
        }
    }, [chrono, Time])

    return (
        <div>
            <span style={{color:color}}>{chrono}</span>
        </div>
    )
}
export default Chrono
